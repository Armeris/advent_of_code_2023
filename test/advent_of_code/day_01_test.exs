defmodule AdventOfCode.Day01Test do
  use ExUnit.Case

  import AdventOfCode.Day01

  @input AdventOfCode.Input.get!(01, 2023)

  test "part1" do
    result = part1(@input)

    assert result == 56465
  end

  test "part2" do
    result = part2(@input)

    assert result == 55902
  end
end
