defmodule AdventOfCode.Day01 do
  @composed_numbers_correspondences [
    oneight: "18",
    twone: "21",
    eightwo: "82",
    eightree: "83"
  ]

  @number_correspondences [
    one: "1",
    two: "2",
    three: "3",
    four: "4",
    five: "5",
    six: "6",
    seven: "7",
    eight: "8",
    nine: "9"
  ]

  @composed_numbers [
    "oneight",
    "twone",
    "eightwo",
    "eighthree",
    "eightwone"
  ]

  @numbers [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine"
  ]

  def part1(input) do
    String.trim_trailing(input)
    |> String.split("\n")
    |> Enum.map(fn line ->
      take_number(line)
    end)
    |> Enum.sum()
  end

  def part2(input) do
    String.trim_trailing(input)
    |> String.split("\n")
    |> Enum.map(fn line ->
      convert_to_nums(line)
      |> take_number()
    end)
    |> Enum.sum()
  end

  @doc """
  Given a list of number in words format and a line of text, returns the numbers that are found in the line of text
  """
  defp find_words_to_replace(number_list, line) do
    Enum.filter(number_list, fn num ->
      String.contains?(line, num)
    end)
  end

  @doc """
  Given a line of text search for numbers written with letters and replaces them with their numeric representation.
  """
  defp convert_to_nums(line) do
    numbers_to_replace =
      find_words_to_replace(@composed_numbers, line)

    first_pass = replace_words_with_numbers(line, numbers_to_replace)

    numbers_to_replace = find_words_to_replace(@numbers, first_pass)

    replace_words_with_numbers(first_pass, numbers_to_replace)
  end

  defp replace_words_with_numbers(input, []) do
    input
  end

  defp replace_words_with_numbers(input, [h | []]) do
    case Keyword.get(@composed_numbers_correspondences, String.to_atom(h)) do
      nil ->
        String.replace(input, h, Keyword.get(@number_correspondences, String.to_atom(h)))

      num ->
        String.replace(input, h, num)
    end
  end

  defp replace_words_with_numbers(input, [h | tail]) do
    replace_words_with_numbers(input, [h])
    |> replace_words_with_numbers(tail)
  end

  defp take_number([num | []]) do
    num
  end

  defp take_number(line) do
    first = Regex.run(~r/[[:digit:]]/, line) |> Enum.take(1)
    last = Regex.scan(~r/[[:digit:]]/, String.reverse(line)) |> Enum.take(1)
    String.to_integer("#{first}#{last}")
  end
end
