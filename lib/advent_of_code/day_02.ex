defmodule AdventOfCode.Day02 do
  @cubes [red: 12, green: 13, blue: 14]

  def part1(input) do
    String.trim_trailing(input)
    |> String.split("\n", trim: true)
    |> Enum.map(&get_game_value(&1))
    |> Enum.sum()
  end

  def part2(input) do
    String.trim_trailing(input)
    |> String.split("\n", trim: true)
    |> Enum.map(&get_game_power(&1))
    |> Enum.sum()
  end

  defp get_game_power(line) do
    [_, match] =
      String.split(line, ":", trim: true)

    String.split(match, ";", trim: true)
    |> Enum.map(fn set ->
      String.split(set, ",", trim: true)
      |> Enum.reduce([red: 0, green: 0, blue: 0], fn color, acc ->
        [power, color] =
          String.split(color, " ", trim: true)

        Keyword.put(acc, String.to_atom(color), String.to_integer(power))
      end)
    end)
    |> get_max_powers()
    |> Enum.reduce(1, fn pow, acc -> pow * acc end)
  end

  defp get_max_powers(kwlist) do
    red = Enum.map(kwlist, fn red -> Keyword.get(red, :red) end) |> Enum.max()
    green = Enum.map(kwlist, fn green -> Keyword.get(green, :green) end) |> Enum.max()
    blue = Enum.map(kwlist, fn blue -> Keyword.get(blue, :blue) end) |> Enum.max()
    [red, blue, green]
  end

  defp get_game_value(line) do
    [game, match] =
      String.split(line, ":", trim: true)

    [_, num] = String.split(game, " ", trim: true)
    game = String.to_integer(num)

    case test_match(match) do
      false -> 0
      true -> game
    end
  end

  defp test_match(match) do
    String.split(match, ";", trim: true)
    |> Enum.all?(fn set ->
      String.split(set, ",", trim: true)
      |> Enum.all?(fn color_cubes ->
        [num, color] = String.split(color_cubes, " ", trim: true)
        test_set(String.to_integer(num), color)
      end)
    end)
  end

  defp test_set(num, color) do
    Keyword.get(@cubes, String.to_atom(color)) >= num
  end
end
